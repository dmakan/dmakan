# Hello, C'est Makan !
Développeur web / mobile (Niveau 5, bac+2)

### Passionné de la programmation
- Je suis actuellement en apprentissage <strong>DevOps</strong>, <strong>GitLab CI / CD</strong>
- Je partage mes XP sur > <a href="www.makandianka.org">www.makandianka.org</a>
- Comment me contacter > <a href="www.makandianka.org/contact">www.makandianka.org/contact</a>

### Social Network :
<a href="https://www.twitter.com/mak_dianka">
    <img src="https://help.twitter.com/content/dam/help-twitter/brand/logo.png" width='60' height='60' />
</a>

<a href="https://www.instagram.com/geek.py">
    <img src="https://upload.wikimedia.org/wikipedia/commons/9/95/Instagram_logo_2022.svg" width='30' height='60' />
</a>

<a href="https://linkedin.com/in/makan-dianka-21b001196">
    <img src="https://legroupeti.com/wp-content/uploads/2018/07/linken.png" width='60' height='60' />
</a>



## Soft Skills
<div>
  <img src="https://www.redhat.com/cms/managed-files/tux-327x360.png" width='120' height='150' />
</div> 

<br />

<div>
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1200px-Python-logo-notext.svg.png" width='60' height='60' />
  
  <img src="https://cdn.coderons.com/general/tagsall/e6923760-0da2-48d7-b801-ec01ace94c95.png" width='60' height='60' />
   
  <img src="https://logodownload.org/wp-content/uploads/2022/04/javascript-logo-4.png" width='60' height='60' />
  
  <img src="https://cdn-icons-png.flaticon.com/512/5969/5969059.png" width='60' height='60' />
 
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1200px-PHP-logo.svg.png" width='60' height='60' />
  
  <img src="https://github.com/symfony.png" width='60' height='60' />

  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/640px-Angular_full_color_logo.svg.png" width='60' height='60' />

  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1200px-Visual_Studio_Code_1.35_icon.svg.png" width='60' height='60' />
</div>

# Support
### Buy me a coffee :

<a href="https://www.buymeacoffee.com/makandianka">
    <img src="https://play-lh.googleusercontent.com/aMb_Qiolzkq8OxtQZ3Af2j8Zsp-ZZcNetR9O4xSjxH94gMA5c5gpRVbpg-3f_0L7vlo" width='60' height='60' />
</a>
